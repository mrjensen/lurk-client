function character(data) {
  let buf = new Buffer.alloc(48 + data.descLen);

  buf.writeUInt8(10, 0);
  buf.write(data.name, 1, 32);
  buf.writeUInt8(data.flags, 33);
  buf.writeUInt16LE(data.attack, 34);
  buf.writeUInt16LE(data.defense, 36);
  buf.writeUInt16LE(data.regen, 38);
  buf.writeInt16LE(0, 40);
  buf.writeUInt16LE(0, 42);
  buf.writeUInt16LE(0, 44);
  buf.writeUInt16LE(data.descLen, 46);
  buf.write(data.desc, 48, data.descLen);

  return buf;
}
module.exports.character = character;

function message(data) {
  let buf = new Buffer.alloc(67 + data.msgLen);

  buf.writeUInt8(1, 0);
  buf.writeUInt16LE(data.msgLen, 1);
  buf.write(data.recpName, 2, 32);
  buf.write(data.sendName, 34, 32);
  buf.write(data.msg, 66, data.msgLen);

  return buf;
}
module.exports.message = message;

function changeroom(data) {
  let buf = new Buffer.alloc(3);

  buf.writeUInt8(2, 0);
  buf.writeUInt16LE(data.roomNum, 1);

  return buf;
}
module.exports.changeroom = changeroom;

function pvp(data) {
  let buf = new Buffer.alloc(33);

  buf.writeUInt8(4, 0);
  buf.write(data.name, 1, 32);

  return buf;
}
module.exports.pvp = pvp;

function loot(data) {
  let buf = new Buffer.alloc(33);

  buf.writeUInt8(5, 0);
  buf.write(data.name, 1, 32);

  return buf;
}
module.exports.loot = loot;
