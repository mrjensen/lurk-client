function decode(n, data) {
  switch (n) {
    case 1:
    return message(data);
    break;

    case 2:
    return changeroom(data);
    break;

    case 3:
    return fight(data);
    break;

    case 4:
    return pvpfight(data);
    break;

    case 5:
    return loot(data);
    break;

    case 6:
    return start(data);
    break;

    case 7:
    return error(data);
    break;

    case 8:
    return accept(data);
    break;

    case 9:
    return room(data);
    break;

    case 10:
    return character(data);
    break;

    case 11:
    return game(data);
    break;

    case 12:
    return leave(data);
    break;

    case 13:
    return connection(data);
    break;

    default:
    return('Error');
  }
}
module.exports.decode = decode;


function message(data) {
  let msgLen = data.readUInt16LE(0);
  if (data.length < 66 + msgLen) return {wait: true};

  return {
    type: 'message',
    end: 66 + msgLen,
    msgLen: msgLen,
    recpName: data.toString('utf8', 2, 32),
    sendName: data.toString('utf8', 34, 32),
    msg: data.toString('utf8', 66, 66 + msgLen)
  };
}

function changeroom(data) { // Not used by server
  return {
    type: 'changeroom',
    end: 2,
    room: data.readUInt16LE(0)
  };
}

function fight(data) { // Not used by server
  return {};
}

function pvpfight(data) { // Not used by server
  return {};
}

function loot(data) { // Not used by server
  return {};
}

function start(data) { // Not used by server
  return {};
}

function error(data) {
  let errLen = data.readUInt16LE(1);
  if (data.length < 3 + errLen) return {wait: true};

  return {
    type: 'error',
    end: 3 + errLen,
    errCode: data.readUInt8(0),
    errLen: data.readUInt16LE(1),
    errMsg: data.toString('utf8', 3, 3 + errLen)
  };
}

function accept(data) {
  return {
    type: 'accept',
    end: 1,
    accepted: data.readUInt8(0)
  };
}

function room(data) {
  let descLen = data.readUInt16LE(34);
  if (data.length < 36 + descLen) return {wait: true};

  return {
    type: 'room',
    end: 36 + descLen,
    roomNum: data.readUInt16LE(0),
    roomName: data.toString('utf8', 2, 32),
    descLen: descLen,
    desc: data.toString('utf8', 36, 36 + descLen)
  };
}

function character(data) {
  let descLen = data.readUInt16LE(45);
  if (data.length < 47 + descLen) return {wait: true};

  return {
    type: 'character',
    end: 47 + descLen,
    name: data.toString('utf8', 0, 32),
    flags: {
      alive: data.readUInt8(32) & 0b10000000 ? true : false,
      joinBattle: data.readUInt8(32) & 0b01000000 ? true : false,
      monster: data.readUInt8(32) & 0b00100000 ? true : false,
      started: data.readUInt8(32) & 0b00010000 ? true : false,
      ready: data.readUInt8(32) & 0b00001000 ? true : false,
      u1: data.readUInt8(32) & 0b00000100 ? true : false,
      u2: data.readUInt8(32) & 0b00000010 ? true : false,
      u3: data.readUInt8(32) & 0b00000001 ? true : false
    },
    attack: data.readUInt16LE(33),
    defense: data.readUInt16LE(35),
    regen: data.readUInt16LE(37),
    health: data.readInt16LE(39),
    gold: data.readUInt16LE(41),
    roomNum: data.readUInt16LE(43),
    descLen: descLen,
    desc: data.toString('utf8', 47, 47 + descLen)
  };
}

function game(data) {
  let descLen = data.readUInt16LE(4);
  if (data.length < 6 + descLen) return {wait: true};

  return {
    type: 'game',
    end: 6 + descLen,
    initialPoints: data.readUInt16LE(0),
    statLimit: data.readUInt16LE(2),
    descLen: descLen,
    desc: data.toString('utf8', 6, 6 + descLen)
  };
}

function leave(data) { // Not used by server
  return {};
}

function connection(data) {
  let descLen = data.readUInt16LE(34);
  if (data.length < 36 + descLen) return {wait: true};

  return {
    type: 'connection',
    end: 36 + descLen,
    roomNum: data.readUInt16LE(0),
    roomName: data.toString('utf8', 1, 32),
    descLen: descLen,
    desc: data.toString('utf8', 36, 36 + descLen)
  };
}
