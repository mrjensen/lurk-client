const term = require('terminal-kit').terminal;

const encode = require('./encode');
const index = require('./index');

let current;

term.on('key', function(name, matches, data) {
  if (name === 'CTRL_C') { send(new Buffer.from([12])); setTimeout(() => { process.exit(); }, 1000) }
  if (name === 'CTRL_ALT_V') { print(255, 255, 255, index.buff.toString(), true); }
  if (name === 'CTRL_Q') { doMessage(); };
  if (name === 'CTRL_W') { doChangeroom(); };
  if (name === 'CTRL_E') { doFight(); };
  if (name === 'CTRL_A') { doPVP(); };
  if (name === 'CTRL_S') { doLoot(); };
  if (name === 'CTRL_D') { doCharacter(); };
  if (name === 'CTRL_X') { doClear(); };
});

function createCharacter(stats) {
  return new Promise((resolve, reject) => {
    let char = {}
    let current;

    getString('Name: ', 'Invalid name.', 32, '^\\w{1,32}$').then((res) => {
      char.name = res;
      getBool('Auto-join battles? [Y|n]').then((res) => {
        char.flags = 0b00000000 + (0b01000000 * res);
        getInt('Attack: ', 0, stats).then((res) => {
          char.attack = res;
          getInt('Defense: ', 0, stats - char.attack).then((res) => {
            char.defense = res;
            getInt('Regen: ', 0, stats - char.attack - char.defense).then((res) => {
              char.regen = res;
              getString('Description: ', 'Invalid description.').then((res) => {
                char.desc = res;
                char.descLen = char.desc.length;
                //term.grabInput(false);
                resolve(char);
              });
            });
          });
        });
      });
    });
  });
}
module.exports.createCharacter = createCharacter;

function start() {
  return new Promise((resolve, reject) => {
    getBool('Start? Choosing no will close the connection [Y|n]\n').then(res => {
      //term.grabInput(false);
      resolve(res);
    });
  });
}
module.exports.start = start;

function doMessage() {
  let msg = {};

  getString('To: ', '', 32).then(res => {
    msg.recpName = res;
    msg.sendName = index.getCharacter().name;
    getString('Message: ', '', -1).then(res => {
      msg.msg = res;
      msg.msgLen = res.length;
      send(encode.message(msg));
    });
  });
}
module.exports.doMessage = doMessage;

function doChangeroom() {
  let changeroom = {};

  for (let room of index.getConnections()) {
    print(255, 255, 0, '#' + room.roomNum + '  ' + room.roomName, true, false);
  }

  getInt('Room number: ').then(res => {
    changeroom.roomNum = res;
    index.moveConnections();
    send(encode.changeroom(changeroom));
  });
}
module.exports.doChangeroom = doChangeroom;

function doFight() {
  send(new Buffer.from([3]));
}
module.exports.doFight = doFight;

function doPVP() {
  let pvp = {};

  for (let other of index.getOthers()) {
    if (other.flags.monster) {
      print(255, 255, 0, other.name, true, false);
    }
  }

  getString('Name: ', '', 32).then(res => {
    pvp.name = res;
    send(encode.pvp(pvp));
  });
}
module.exports.doPVP = doPVP;

function doLoot() {
  let loot = {};

  for (let other of index.getOthers()) {
    if (other.health <= 0) {
      print(255, 255, 0, other.name, true, false);
    }
  }

  getString('Name: ', '', 32).then(res => {
    loot.name = res;
    send(encode.loot(loot));
  });
}
module.exports.doLoot = doLoot;

function doCharacter() {
  let char = {};

  print(255, 255, 0, '-----CURRENT-----', true, true);
  print(255, 255, 0, 'Auto-join: ' + index.getCharacter().flags.joinBattle ? 'true' : 'false', true, true);
  print(255, 255, 0, 'Attack: ' + index.getCharacter().attack, true, true);
  print(255, 255, 0, 'Defense: ' + index.getCharacter().defense, true, true);
  print(255, 255, 0, 'Regen: ' + index.getCharacter().regen, true, true);
  print(255, 255, 0, 'Description: ' + index.getCharacter().desc, true, true);
  print(255, 255, 0, '-----CURRENT-----', true, true);

  getBool('Auto-join battles? [Y|n]').then(res => {
    char.flags = 0b00000111 + (0b01000000 * res);
    getInt('Attack: ', 0, index.GAME.statLimit).then(res => {
      char.attack = res;
      getInt('Defense: ', 0, index.GAME.statLimit - char.attack).then(res => {
        char.defense = res;
        getInt('Regen: ', 0, index.GAME.statLimit - char.attack - char.defense).then(res => {
          char.regen = res;
          getString('Description: ', '', -1).then(res => {
            char.desc = res;
            char.descLen = res.length;
            char.name = index.getCharacter().name;

            send(encode.character(char));
          });
        });
      });
    });
  });
}
module.exports.doCharacter = doCharacter;

function doClear() {
  term.clear();
  print(0, 0, 0, '', false, false, true);
}
module.exports.doClear = doClear;










function getString(str, errMsg, maxLen, rgx) {
  return new Promise((resolve, reject) => {
    let options = {};
    if (maxLen > 0) options = {maxLength: maxLen};

    term.grabInput(false);
    if (current) current.abort();
    print(0, 255, 0, str, false, true);
    current = term.inputField(options, (err, input) => {
      if (!input) resolve('');
      if (rgx) {
        if (input.match(new RegExp(rgx))) {
          resolve(input);
        } else {
          print(255, 0, 0, errMsg, false);
          getString(str, errMsg, maxLen, rgx).then(res => { resolve(res) });
        }
      } else {
        resolve(input);
      }
    });
  });
}

function getBool(str) {
  return new Promise((resolve, reject) => {

    term.grabInput(false);
    if (current) current.abort();
    print(0, 255, 0, str, false, true);
    current = term.yesOrNo({yes: [ 'y', 'ENTER'], no: ['n']}, (err, input) => {
      if (input) resolve(true);
      resolve(false);
    });
  });
}

function getInt(str, min, max) {
  return new Promise((resolve, reject) => {

    term.grabInput(false);
    if (current) current.abort();
    print(0, 255, 0, str, false, true);
    current = term.inputField((err, input) => {
      if (!input) resolve(0);
      else if (input.match(/^[0-9]+$/g)) { //It is an integer
        let n = parseInt(input);
        if (min && n < min) {
          print(255, 0, 0, 'Number is too small.', false);
          getInt(str, min, max).then(res => { resolve(res) });
        } else if (max && n > max) {
          print(255, 0, 0, 'Number is too large.', false);
          getInt(str, min, max).then(res => { resolve(res) });
        } else {
          resolve(n);
        }
      } else {
        print(255, 0, 0, 'Input must be an integer.', false);
        getInt(str, min, max).then(res => { resolve(res) });
      }
    });
  });
}






function print(r, g, b, string, nl, bold, noline) {
  term.colorRgb(r,g,b);
  if (bold) term.bold();
  term((noline ? '' : '\n') + string);
  if (nl) term('\n');
  term.defaultColor().styleReset();

  term.saveCursor();
  term.moveTo.bgBrightWhite.black(1, 1).eraseLine();
  term('CTRL -- Q: Message W: Changeroom E: Fight A: PVP S: Loot D: Character X: Clear');
  term.moveTo.bgBrightWhite.black(1, 2).eraseLine();
  term('Health: ' + index.getCharacter().health + ' Room: ' + index.getCharacter().roomNum + ' Gold: ' + index.getCharacter().gold + ` Attack: ${index.getCharacter().attack} Defense: ${index.getCharacter().defense} Regen: ${index.getCharacter().regen}`);
  term.defaultColor.bgDefaultColor();
  term.restoreCursor();
}
module.exports.print = print;

function send(data) {
  index.sock.write(data);
}
module.exports.send = send;
