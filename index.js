const net = require('net');
const buffer = require('buffer');

const decode = require('./decode');
const encode = require('./encode');
const game = require('./game');

const sock = new net.Socket();
module.exports.sock = sock;
let buff = new Buffer.alloc(0);
module.exports.buff = buff;

let GAME = {}, CHARACTER = {}, ROOM = {}, CONNECTIONS = [], LASTCONNECTIONS = [], OTHERS = [];
module.exports.GAME = GAME; module.exports.CHARACTER = CHARACTER; module.exports.ROOM = ROOM; module.exports.CONNECTIONS = CONNECTIONS; module.exports.LASTCONNECTIONS = LASTCONNECTIONS; module.exports.OTHERS = OTHERS;

if (!process.argv[2] || !process.argv[3]) {
  require('terminal-kit').terminal.colorRgb(255, 0, 0).bold('Include a hostname and port.\n');
  process.exit();
}
const HOST = process.argv[2], PORT = parseInt(process.argv[3]);

sock.connect(PORT, HOST);


sock.on('connect', () => {
  console.log('Connected to ' + sock.remoteAddress + ':' + sock.remotePort);
  // sock.destroy(); // Instantly destroy the socket. Used to test if it connects properly.
});

sock.on('data', data => {
  buff = Buffer.concat([buff, new Buffer.from(data)]);
  checkData();
});


function checkData() {
  if (buff.length == 0) return;
  let type = buff.readUInt8(0);
  let json = decode.decode(type, buff.slice(1));

  if (json.wait) return;
  else {
    buff = buff.slice(json.end + 1);
    checkData();
  }

  switch(json.type) {
    case 'message':
    game.print(255, 128, 0, '-----MESSAGE-----', true, true);
    game.print(255, 128, 0, 'From: ', false, true, true);
    game.print(255, 128, 0, json.sendName || 'Server', true, false, true);
    game.print(255, 128, 0, 'Message: ', false, true, true);
    game.print(255, 128, 0, json.msg, true, false, true);
    break;
    case 'error':
    game.print(255, 0, 255, 'Error #' + json.errCode + ': ' + json.errMsg, true, true);

    if (json.errCode == 1) CONNECTIONS = LASTCONNECTIONS;
    break;
    case 'accept':
    game.print(100, 100, 100, 'Accept: ', false, true);
    game.print(100, 100, 100, json.accepted, true, false, true);
    if (json.accepted == 10) {
      game.start().then(res => {
        if (res) game.send(new Buffer.from([6]))
        else {
          print(255, 0, 0, 'Closing connection...', true, true, false);
          game.send(new Buffer.from([12]));
          setTimeout(() => {
            process.exit();
          }, 1000);
        }
      });
    }
    break;
    case 'room':
    ROOM = json;
    game.print(0, 128, 255, '-----ROOM-----', true, true);
    game.print(0, 128, 255, '#' + json.roomNum + '  Name: ', false, true, true);
    game.print(0, 128, 255, json.roomName, true, false, true);
    game.print(0, 128, 255, 'Description: ', false, true, true);
    game.print(0, 128, 255, json.desc, true, false, true);
    break;
    case 'character':
    if (!CHARACTER.name || json.name == CHARACTER.name) {
      CHARACTER = json;
      break;
    }
    else if (json.room == CHARACTER.room) OTHERS.push(json);

    game.print(128, 255, 0, '-----CHARACTER-----', true, true);
    game.print(128, 255, 0, 'Name: ', false, true, true);
    game.print(128, 255, 0, json.name, true, false, true);
    game.print(128, 255, 0, 'Alive: ', false, true, true);
    game.print(128, 255, 0, json.flags.alive, false, false, true);
    game.print(128, 255, 0, ' Monster: ', false, true, true);
    game.print(128, 255, 0, json.flags.monster, true, false, true);
    game.print(128, 255, 0, 'Health: ', false, true, true);
    game.print(128, 255, 0, json.health, false, false, true);
    game.print(128, 255, 0, ' Gold: ', false, true, true);
    game.print(128, 255, 0, json.gold, true, false, true);
    game.print(128, 255, 0, 'Attack: ', false, true, true);
    game.print(128, 255, 0, json.attack, false, false, true);
    game.print(128, 255, 0, ' Defense: ', false, true, true);
    game.print(128, 255, 0, json.defense, false, false, true);
    game.print(128, 255, 0, ' Regen: ', false, true, true);
    game.print(128, 255, 0, json.regen, true, false, true);
    break;
    case 'game':
    GAME = json;
    game.createCharacter(GAME.initialPoints).then((char) => {
      game.send(encode.character(char));
    });
    break;
    case 'connection':
    CONNECTIONS.push(json);
    game.print(0, 128, 255, '-----CONNECTION-----', true, true);
    game.print(0, 128, 255, '#' + json.roomNum + '  Name: ', false, true, true);
    game.print(0, 128, 255, json.roomName, true, false, true);
    game.print(0, 128, 255, 'Description: ', false, true, true);
    game.print(0, 128, 255, json.desc, true, false, true);
    break;
  }
}

function moveConnections() {
  LASTCONNECTIONS = JSON.parse(JSON.stringify(CONNECTIONS));
  CONNECTIONS = JSON.parse(JSON.stringify([]));
}
module.exports.moveConnections = moveConnections;

function moveOthers() {
  LASTOTHERS = JSON.parse(JSON.stringify(OTHERS));
  OTHERS = JSON.parse(JSON.stringify([]));
}
module.exports.moveOthers = moveOthers;

function getConnections() {
  return CONNECTIONS;
}
module.exports.getConnections = getConnections;

function getOthers() {
  return OTHERS;
}
module.exports.getOthers = getOthers;

function getCharacter() {
  return CHARACTER;
}
module.exports.getCharacter = getCharacter;
